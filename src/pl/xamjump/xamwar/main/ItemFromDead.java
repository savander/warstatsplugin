package pl.xamjump.xamwar.main;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

public class ItemFromDead implements Listener{
	private Main plugin; 
	
	public ItemFromDead(Main plugin) {
		this.plugin = plugin;

	}
	
	
	@SuppressWarnings("static-access")
	@EventHandler
	public void onEntityDeath(PlayerDeathEvent ev) {
			
			Random  random = new Random();
			int ChanceGoldIngot = random.nextInt(100);
			int ChanceBottle = random.nextInt(100);
			int ChanceBottleStack = random.nextInt(2)+1;
		
			
			/// Itemki ze smierci
			Player dead = ev.getEntity();
			try{
				if(ChanceGoldIngot >=30){
					dead.getKiller().getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 1));
				}
				if(ChanceBottle >=25){
					dead.getKiller().getInventory().addItem(new ItemStack(Material.EXP_BOTTLE, ChanceBottleStack));
				}
				dead.getKiller().getInventory().addItem(new ItemStack(Material.DIAMOND, 1));
				
				
				//Kasa z zabicia
				if(dead.getKiller() == null ||  dead.getKiller() == dead.getPlayer());
				else if(dead.getKiller().hasPermission("waritem.svip")){
					plugin.economy.depositPlayer(dead.getKiller().getName(), 20);	
					dead.getKiller().sendMessage("Dostales 20$ za zabicie gracza!");
				}
				else if(dead.getKiller().hasPermission("waritem.vip")){
					plugin.economy.depositPlayer(dead.getKiller().getName(), 15);	
					dead.getKiller().sendMessage("Dostales 15$ za zabicie gracza!");
				}
				else if(dead.getKiller().hasPermission("waritem.normal")){
					plugin.economy.depositPlayer(dead.getKiller().getName(), 10);	
					dead.getKiller().sendMessage("Dostales 10$ za zabicie gracza!");
			}
			}catch(Exception e){
				dead.sendMessage("Samob�jstwo !:O");
			}
		
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onRespawnPlayer(PlayerRespawnEvent evt){
			Player p = evt.getPlayer();
			
			if(p.getInventory().getHelmet() != null){
				if(p.getInventory().getHelmet().getTypeId() == 310){
						p.getInventory().getHelmet().setDurability((short)(p.getInventory().getHelmet().getDurability() + 40));
						if(p.getInventory().getHelmet().getDurability() >= 363){
							p.getInventory().setHelmet(null);
						}
					}
					else if(p.getInventory().getHelmet().getTypeId() == 302 || p.getInventory().getHelmet().getTypeId() == 306){
						p.getInventory().getHelmet().setDurability((short)(p.getInventory().getHelmet().getDurability() + 20));
						if(p.getInventory().getHelmet().getDurability() >= 165){
							p.getInventory().setHelmet(null);
						}
					}else{
						p.getInventory().getHelmet().setDurability((short)(p.getInventory().getHelmet().getDurability() + 10));
						if(p.getInventory().getHelmet().getDurability() >= 55){
							p.getInventory().setHelmet(null);
						}
					}
			}
			
			if(p.getInventory().getChestplate() != null){
				if(p.getInventory().getChestplate().getTypeId() == 311){
						p.getInventory().getChestplate().setDurability((short)(p.getInventory().getChestplate().getDurability() + 55));
						if(p.getInventory().getChestplate().getDurability() >= 528){
							p.getInventory().setChestplate(null);
						}
					}
					else if(p.getInventory().getChestplate().getTypeId() == 303 || p.getInventory().getChestplate().getTypeId() == 307){
						p.getInventory().getChestplate().setDurability((short)(p.getInventory().getChestplate().getDurability() + 35));
						if(p.getInventory().getChestplate().getDurability() >= 240){
							p.getInventory().setChestplate(null);
						}
					}else{
						p.getInventory().getChestplate().setDurability((short)(p.getInventory().getChestplate().getDurability() + 15));
						if(p.getInventory().getChestplate().getDurability() >= 80){
							p.getInventory().setChestplate(null);
						}
					}
			}
					

			if(p.getInventory().getLeggings() != null){
				if(p.getInventory().getLeggings().getTypeId() == 312){
						p.getInventory().getLeggings().setDurability((short)(p.getInventory().getLeggings().getDurability() + 52));
						if(p.getInventory().getLeggings().getDurability() >= 495){
							p.getInventory().setLeggings(null);
						}
					}
					else if(p.getInventory().getLeggings().getTypeId() == 304 || p.getInventory().getLeggings().getTypeId() == 308){
						p.getInventory().getLeggings().setDurability((short)(p.getInventory().getLeggings().getDurability() + 32));
						if(p.getInventory().getLeggings().getDurability() >= 225){
							p.getInventory().setLeggings(null);
						}
					}else{
						p.getInventory().getLeggings().setDurability((short)(p.getInventory().getLeggings().getDurability() + 12));
						if(p.getInventory().getLeggings().getDurability() >= 75){
							p.getInventory().setLeggings(null);
						}
					}
			}
					
			if(p.getInventory().getBoots() != null){
				if(p.getInventory().getBoots().getTypeId() == 313){
						p.getInventory().getBoots().setDurability((short)(p.getInventory().getBoots().getDurability() + 48));
						if(p.getInventory().getBoots().getDurability() >= 429){
							p.getInventory().setBoots(null);
						}
					}
					else if(p.getInventory().getBoots().getTypeId() == 305 || p.getInventory().getBoots().getTypeId() == 309){
						p.getInventory().getBoots().setDurability((short)(p.getInventory().getBoots().getDurability() + 28));
						if(p.getInventory().getBoots().getDurability() >= 195){
							p.getInventory().setBoots(null);
						}
					}else{
						p.getInventory().getBoots().setDurability((short)(p.getInventory().getBoots().getDurability() + 12));
						if(p.getInventory().getBoots().getDurability() >= 65){
							p.getInventory().setBoots(null);
						}
					}
			}
					



					
	}
}
		

	
