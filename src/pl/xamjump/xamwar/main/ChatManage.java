package pl.xamjump.xamwar.main;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatManage implements Listener, CommandExecutor {
	@SuppressWarnings("unused")
	private Main plugin; 
	public boolean chat = true;
	
	public ChatManage(Main plugin) {
		this.plugin = plugin;
		
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player) sender;
		if(sender instanceof Player){
			if(label.equalsIgnoreCase("warchat") && p.hasPermission("warchat.manage")){
				try{
				if(args[0].equalsIgnoreCase("off")){
					if(chat == false){
						p.sendMessage("Chat juz jest wylaczony");
					}else{
						for(int i=0;i<10;i++){
							Bukkit.broadcastMessage("");
							if(i == 0){
								Bukkit.broadcastMessage("�c<------Chat zostal wylaczony!------>");
							}
						}
					chat = false;
					}
				}
				
				else if(args[0].equalsIgnoreCase("on")){
					
					if(chat == true){
						p.sendMessage("Chat juz jest wlaczony");
					}else{
						for(int i=0;i<5;i++){
							Bukkit.broadcastMessage("");
							if(i == 0){
								Bukkit.broadcastMessage("�c<------Chat zostal wlaczony!------>");
							}
						}
						chat = true;	
					}
				}else{
					p.sendMessage("/warchat {off/on}");
				}
				}catch(Exception e){
					p.sendMessage("/warchat {off/on}");
				}
					return false;
			}else{
					p.sendMessage("�cYou don't have permissions");
					return false;
				}
			
		}
		
		
		return false;
	}
	
	
	@EventHandler
	public void chatManage(AsyncPlayerChatEvent evt){
		if(chat == true){
			evt.setCancelled(false);
		}else if(chat == false){
			if(!(evt.getPlayer().hasPermission("warchat.manage")||evt.getPlayer().hasPermission("warchat.canchat")) ){
				evt.setCancelled(true);
			}else{
				evt.setCancelled(false);
			}
			
		}
	}
	
}
