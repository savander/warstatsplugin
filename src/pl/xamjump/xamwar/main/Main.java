package pl.xamjump.xamwar.main;

import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	protected static final Logger Log =  Logger.getLogger("Minecraft");
    public static Economy economy = null;

	@Override
	public void onEnable() {
		Log.info(this.getName()+ " enabled");
		this.setupEconomy();
		getServer().getPluginManager().registerEvents(new ItemFromDead(this), this);

		
		this.getCommand("warchat").setExecutor(new ChatManage(this));
	}

	
	public boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
}



